<?php

namespace App\Console\Commands;

use App\CategoriesModel;
use Illuminate\Console\Command;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;

class MwananchiStoreCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mwananchistore:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command adds & updates Mwananchi Store Categories  ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating Categories List. Hold On!');

        $file = app_path().'/storeCategoriesYml.yml';

        if ( ! file_exists($file))
        {
            $this->error('Ohhh! Unfortunate the file storeCategoriesYml does not exist');
        }
        else{
            //Load the YAML file and parse it.
            $array = Yaml::parse(file_get_contents($file));

            foreach ($array as $key => $value) {
                $title = $key;
                //Lets loop through the value array to get the other details.
                $machine_name = $value['title'];
                $sub_categories = $value['sub_categories'];
                $description = $value['description'];
                $icon = $value['icon'];
                $line_divider = $value['line_divider'];

                //We need to check if we already stored this category.
                $category = CategoriesModel::where('machine_name','=',$machine_name)->first();

                //Only create a new permission if it is not existing.
                if (empty($category)) {
                    $new_category = new CategoriesModel;
                    $new_category->title = $title;
                    $new_category->sub_categories = $sub_categories;
                    $new_category->description = $description;
                    $new_category->icon = $icon;
                    $new_category->line_divider = $line_divider;
                    $new_category->machine_name = $machine_name;
                    $new_category->save();

                    $this->info('New Category:'.$title);
                }
            }
        }
    }
}
