<?php

namespace App\Console\Commands;

use App\BrandsModel;
use Illuminate\Console\Command;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;

class MwananchiStoreBrands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mwananchistore:brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command adds & updates Mwananchi Store Brands';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating Brands List. Hold On!');

        $file = app_path().'/storeBrandsYml.yml';

        if ( ! file_exists($file))
        {
            $this->error('The file storeBrandsYml.yml does not exist');
        }
        else{
            //Load the YAML file and parse it.
            $array = Yaml::parse(file_get_contents($file));

            foreach ($array as $key => $value) {
                $name = $key;
                //Lets loop through the value array to get the other details.
                $machine_name = $value['name'];
                $description = $value['description'];

                //We need to check if we already stored this category.
                $category = BrandsModel::where('machine_name','=',$machine_name)->first();

                //Only create a new permission if it is not existing.
                if (empty($category)) {
                    $new_category = new BrandsModel;
                    $new_category->name = $name;
                    $new_category->machine_name = $machine_name;
                    $new_category->description = $description;
                    $new_category->save();

                    $this->info('New Category:'.$name);
                }
            }
        }
    }
}
