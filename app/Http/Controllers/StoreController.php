<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;

use App\Store;
use App\BrandsModel;
use App\CategoriesModel;
use App\PhotoModel;

use Image;
use Carbon\Carbon;

class StoreController extends Controller
{

    /*Handles storage of new items*/
	public function storeItems(Request $request) {

    	$items = new Store;
        $random_id = Store::orderBy('iid', 'DESC')->take(1)->get();
        if ($random_id->isEmpty()) {
            $items->iid = 1;
        } else {
            foreach ($random_id as $count) {
                $id = $count->iid;
                $items->iid = $id + 1;
            }
        }
        $items->item_title = $request->input('item_title');
        $items->item_price = $request->input('item_price');
        $items->item_category = $request->input('item_category');
        $items->category_snake_case = snake_case($request->input('category_snake_case'));
        $items->item_description = $request->input('item_description');
        $items->user_location = $request->input('user_location');
        $items->item_photos = $request->input('item_photos');
        $items->item_sub_date = date('M d, Y');

        $items->user_name = $request->input('user_name');
        $items->user_mobile = $request->input('user_mobile');
        $items->user_email = $request->input('user_email');

        $items->agree_to_terms = $request->input('agree_to_terms');
 		$items->save();

 		//Create user account for the first time. / Store only once.
 		/*$checkUser = Users::orderBy('uid', 'DESC')->take(1)->get();
 		if ($checkUser->isEmpty()) {

 		
 		}else{

 			$items = new Users;
			$random_uid = Users::orderBy('uid', 'DESC')->take(1)->get();
	        if ($random_uid->isEmpty()) {
	            $user->uid = 1;
	        } else {
	            foreach ($random_uid as $count) {
	                $id = $count->uid;
	                $user->uid = $id + 1;
	            }
	        }
	        $items->user_name = Input::get('user_name');
	        $items->user_mobile = Input::get('user_mobile');
	        $items->user_email = Input::get('user_email');
	        $items->user_reg_date = date('M d, Y');
	        $items->save();

 		}
        */
       
        return response($items);
      
    }

    public function storePhotos(Request $request){ 


            $files = $request->file('image');
                    
                if($request->hasFile('image')) {

                    foreach ($request->file('image') as $key => $getfile) { 

                        //Bind time stamp
                        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                        $fileName = $timestamp. '-' .$getfile->getClientOriginalName();

                        //Store photo Path
                        $store = new PhotoModel;
                        $store->photo_path = $fileName;
                        $store->save();

                        //Perform some Intervention
                        $getfile->move('images/uploads/', $fileName);
                        $image = Image::make(sprintf('images/uploads/%s', $fileName))
                        ->insert('images/watermark/itemPhoto.png', 'center')
                        ->resize(300, 200)
                        ->save();

                    }

                }


            return Redirect::to('/');

    }

    /*Return Data for our search engine*/
    public function searchStore(){
        return Response::json(Store::get());
    }
    /*Fetch category data*/
    public function fetchCategories(){
        return Response::json(CategoriesModel::get());
    }

    public function fetchBrands(){
        return Response::json(BrandsModel::get());
    }

}