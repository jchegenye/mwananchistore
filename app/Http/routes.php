<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
    
    Route::get('/', function () {
        return view('app');
    });

    Route::post('/new-photo', [
        'uses' => 'StoreController@storePhotos'
    ]);
    
});


    Route::resource('items/stored', 'StoreController@searchStore');

    Route::resource('items', 'StoreController@storeItems');

    Route::resource('categories', 'StoreController@fetchCategories');

    Route::resource('brands', 'StoreController@fetchBrands');

    // Templates
    Route::group(array('prefix'=>'/templates/'),function(){
        Route::get('{template}', array( function($template)
        {
            $template = str_replace(".html","",$template);
            View::addExtension('html','php');
            return View::make('templates.'.$template);
        }));
    });
    
    // Templates/itemPartials
    Route::group(array('prefix'=>'/templates/itemPartials/segmentOne'),function(){
        Route::get('{template}', array( function($template)
        {
            $template = str_replace(".html","",$template);
            View::addExtension('html','php');
            return View::make('templates.itemPartials.segmentOne.'.$template);
        }));
    });

    Route::group(array('prefix'=>'/templates/itemPartials/latest'),function(){
        Route::get('{template}', array( function($template)
        {
            $template = str_replace(".html","",$template);
            View::addExtension('html','php');
            return View::make('templates.itemPartials.latest.'.$template);
        }));
    });