<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Store extends Eloquent
{
    protected $collection = "items";

    protected $fillable = ['item_title'];
}
