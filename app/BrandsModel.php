<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class BrandsModel extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['machine_name'];

    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $table = "brands";
}
