<!DOCTYPE html>
<html lang="en" ng-app="mwananchiStoreApp">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="The Best Kenyan online shopping website for mobiles, electronics, appliances, lifestyle and wide range of original products with cash on delivery at MwananchiStore.co.ke">
    <meta name="author" content="Jackson A. Chegenye">
    
    <title>Online Ads Kenya | Sell, Buy & Much More Online at MwananchiStore.co.ke</title>

    <link href="/angular-1.5.8/angular-csp.css" rel="stylesheet"><!-- CSP (Content Security Policy) rules. -->
    <link href="/css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap -->
    <link href="/css/font-awesome.min.css" rel="stylesheet"><!-- Font-awesome -->
    <link href="/css/price-range.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">

    <!-- Angular Default Files -->
    <script src="/angular-1.5.8/angular.min.js"></script>
    <script src="/angular-1.5.8/angular-route.min.js"></script>

    <!-- Angular JS MwananchiStore Files -->
    <script src="/app/app.js"></script>
    <script src="/app/controllers/StoreController.js"></script>
    <script src="/app/controllers/SearchStoreController.js"></script>
    <script src="/app/controllers/RatingCtrl.js"></script>
    <script src="/app/controllers/MapCtrl.js"></script>

    <!-- Angular JS libraries & Dependencies -->
    <script src="/node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js"></script>
    <script src="/node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js"></script>
    <script src="/bower_components/flow.js/dist/flow.min.js"></script>
    <script src="/bower_components/ng-flow/dist/ng-flow.min.js"></script>
    <script src="/bower_components/ngmap/build/scripts/ng-map.js"></script>
    <script src="/bower_components/ngstorage/ngStorage.min.js"></script>

    <link rel="shortcut icon" href="images/ico/favicon.png">

    <base href="/" />
</head>

    <!-- Wire up a Laravel Backend --> 
    <body ng-cloak>

        <header id="header">
            <!--Segment 1. [Help, Seller, Social Accounts]-->
            <div class="header_top">
                <div ng-include="'templates/slim_header.html'"></div>
            </div>

            <!--Segment 2. [Logo, Currency, Language, Account, Wishlist, Login]-->
            <div class="header-middle">
                <div ng-include="'templates/header.html'"></div>
            </div>

            <!--Segment 3. [Search Input]-->
            <div class="header-bottom">
                <div ng-include="'templates/search.html'"></div>
            </div>
        </header>

        <!--Segment 4. [Slider]-->
        <section id="slider">
            <div ng-include="'templates/slider.html'"></div>
        </section>

        <!--Segment 5. [Brands, Price Range]-->
        <span ng-controller="SearchStoreController"> <!-- ng-hide="getItemId" to hide ALL-->
            <section id="segment5">
                <div ng-include="'templates/items_listing_one.html'"></div>
            </section>
            <!--Segment 6. [Advert]-->
            <section id="segment6">
                <div ng-include="'templates/items_listing_two.html'"></div>
            </section>
        </span>
       

        <!--Segment 7. [Footer]-->
        <footer id="footer">
            <div ng-include="'templates/footer.html'"></div>
        </footer>

        <!--Segment 8. [Add Items Modal]-->
        <div class="modal fade" id="sell-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div ng-include="'templates/add_item_modal.html'"></div>
        </div>

        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.scrollUp.min.js"></script>
        <script src="/js/price-range.js"></script>
        <script src="/js/main.js"></script>
        <script src='http://maps.googleapis.com/maps/api/js?v=3&amp;libraries=places&key=AIzaSyCq-JG_nRxZBM2JU8JQ8RhdrhWpbZPuoDQ'></script><!-- Google Maps Api Key -->

    </body>

</html>