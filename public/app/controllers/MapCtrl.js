
app.controller('MapCtrl', function(NgMap){
var vm = this;
	NgMap.getMap().then(function(map) {
	  	vm.showCustomMarker= function(evt) {
	    	map.customMarkers.foo.setVisible(true);
	    	map.customMarkers.foo.setPosition(this.getPosition());
	 };
	  	vm.closeCustomMarker= function(evt) {
	    	this.style.display = 'none';
	    	this.style.backgroundColor = "rgba(254, 152, 15, 0.8)";
	    	this.style.color = "#ffffff";
	    	this.style.fontSize = "12px";
	    	this.style.padding = "10px";
	  	};
	});
});