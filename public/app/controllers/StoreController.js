/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE
 *
 * @File Controls Ampath Ordering System <www.j-tech.tech>
 */

app.controller('StoreController', function($scope,$http) {
   
    $scope.obj = {};
    $scope.upload = function () {
        console.log($scope.obj.flow);
    }

    /*$scope.upload = function (id) {

        $scope.obj.flow.opts.testChunks=false;
        $scope.obj.flow.opts.target='/upload';
        $scope.obj.flow.upload();
        console.log($scope.obj.flow);
    }
*/
    /*$scope.InsertItems = function() {
        $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {      
            console.log($flow.obj.query = {id:$scope.id});
        });
    }*/

    $scope.data = []; //declare an empty array
    $scope.saveAdd = function(){

    var result = confirm("Are you sure you want to submit this item?");
        if (result) {
            $http.post('/items',$scope.form).then(function(data) {

                $scope.data.push(data);
                console.log(data);
                $(".modal").modal("hide");

            });
            alert('Your item has been submitted');
        } else{

            alert('Your item submission has been canceled');
        }
        
    }

    /*Fetch category list.*/
    $scope.categories = []; //declare an empty array
    $http.get("/categories").success(function(data){ //mock json data available in database
        $scope.categories = data; //ajax request to fetch data into $scope.data
    });

    /*Fetch brands list.*/
    $scope.brands = []; //declare an empty array
    $http.get("/brands").success(function(data){ //mock json data available in database
        $scope.brands = data; //ajax request to fetch data into $scope.data
    });

   
});

/*Fetch Kenya places using Google Places API*/
app.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {country: 'KE'}
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
            });
        }
    };
});

app.config(['flowFactoryProvider', function (flowFactoryProvider) {
  flowFactoryProvider.defaults = {
    target: '/images',
    permanentErrors: [404, 500, 501],
    maxChunkRetries: 1,
    chunkRetryInterval: 5000,
    simultaneousUploads: 4,
    singleFile: true
  };
  flowFactoryProvider.on('catchAll', function (event) {
    console.log('catchAll', arguments);
  });
  // Can be used with different implementations of Flow.js
  // flowFactoryProvider.factory = fustyFlowFactory;
}]);